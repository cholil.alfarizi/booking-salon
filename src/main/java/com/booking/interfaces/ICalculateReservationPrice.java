package com.booking.interfaces;



public interface ICalculateReservationPrice {
    void calculateReservationPrice();

    double NONE_DISKON = 0.0;
    double SILVER_DISKON = 0.05;
    double GOLD_DISKON = 0.1;
    
}
