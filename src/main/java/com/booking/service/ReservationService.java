package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ReservationService {
    public static void createReservation(List<Person> personList, List<Service> serviceList, List<Reservation> reservationList ){
        Scanner scanner = new Scanner(System.in);
        String custID,empID,serviceID,serviceChoices;
        PrintService printService = new PrintService();
        Customer customerToReserve = new Customer();
        Employee employeeToServe = new Employee();
        Reservation reservation = new Reservation();
        List<Service> serviceListToUse = new ArrayList<>();
        Service service = new Service();

        printService.showAllCustomer(personList);

        do{
            System.out.println("Silahkan Masukkan Customer ID");
            custID = scanner.nextLine();
            customerToReserve = getCustomerByCustomerId(personList,custID);
            if (customerToReserve != null) {
                reservation.setCustomer(customerToReserve);
            }else{
                System.out.println("Tidak ada Customer dengan ID tersebut");
            }
        }while(customerToReserve == null);

        // customerToReserve = getCustomerByCustomerId(personList);

        // if (customerToReserve != null) {
        //     reservation.setCustomer(customerToReserve);
        // }else{
        //     System.out.println("Tidak ada Customer dengan ID tersebut");
        // }

        printService.showAllEmployee(personList);
        do{
            System.out.println("Silahkan Masukkan Employee ID");
            empID = scanner.nextLine();
            employeeToServe = getEmployeeByEmployeeID(personList,empID);
            if (employeeToServe != null) {
                reservation.setEmployee(employeeToServe);
            }else{
                System.out.println("Tidak ada Employee dengan ID tersebut");
            }
        }while(employeeToServe == null);

        // employeeToServe = getEmployeeByEmployeeID(personList);

        // if (employeeToServe != null) {
        //     reservation.setEmployee(employeeToServe);
        // }else{
        //     System.out.println("Tidak ada Employee dengan ID tersebut");
        // }

        printService.showAllService(serviceList);
        outerloop:
        do{
            do{
                System.out.println("Silahkan Masukkan Service Id");
                serviceID = scanner.nextLine();
                service = getServiceByServiceID(serviceList, serviceID);
                if (serviceListToUse.size() == serviceList.size()) {
                    System.out.println("Semua service sudah dipilih silahkan akhiri reservasi");
                    break;
                }
                if (service != null) {
                    if (!serviceListToUse.contains(service)) {
                        serviceListToUse.add(service);
                    }else{
                        System.out.println("service sudah dipilih");
                    }
                    // if (serviceListToUse.size() == serviceList.size()) {
                    //     System.out.println("Semua service sudah dipilih silahkan akhiri reservasi");
                    // }
                    
                }else{
                    System.out.println("Tidak ada Service dengan ID tersebut");
                }
                
            }while (service == null);

            do{
                System.out.println("Ingin pilih service lain (Y/T)");
                serviceChoices = scanner.nextLine();
                if (serviceChoices.equalsIgnoreCase("T")) {
                    System.out.println("Booking berhasil");
                    break outerloop;
                }else if (serviceChoices.equalsIgnoreCase("Y")) {
                    continue outerloop;
                }
            }while (serviceChoices.isEmpty() || !serviceChoices.equalsIgnoreCase("Y") || !serviceChoices.equalsIgnoreCase("T"));
        }while(true);

        reservation.setNewReservationID();
        reservation.setServices(serviceListToUse);
        reservation.setWorkstage("In Process");
        reservation.calculateReservationPrice();
        //reservation.getReservationPrice();

        reservationList.add(reservation);

    }

    public static Customer getCustomerByCustomerId(List<Person> personList, String custID){
        Customer customerToReserve = null;
        for (Person person : personList) {
            if (person instanceof Customer) {
                Customer customer = new Customer();
                customer = (Customer) person;
                if (customer.getId().equalsIgnoreCase(custID)) {
                    customerToReserve = customer;
                    break;
                }
            }
        }
        return customerToReserve;
    }

    public static Employee getEmployeeByEmployeeID(List<Person> personList, String empID){
        Employee employeeToServe = null;
        for (Person person : personList) {
            if (person instanceof Employee) {
                Employee employee = new Employee();
                employee = (Employee) person;
                if (employee.getId().equalsIgnoreCase(empID)) {
                    employeeToServe = employee;
                    break;
                }
            }
        }

        return employeeToServe;
    }

    public static Service getServiceByServiceID(List<Service> serviceList, String serviceID){
        Service serviceChoices = null;
        for (Service service : serviceList) {
            if (service.getServiceId().equalsIgnoreCase(serviceID)) {
                serviceChoices =  service;
                break;
            }
        }
        return serviceChoices;
    }

    public static void editReservationWorkstage(List<Reservation> reservationList ){
        Scanner scanner = new Scanner(System.in);
        String reserveID, reserveEnd;
        PrintService printService = new PrintService();
        
        printService.showRecentReservation(reservationList);

        Reservation reservationToEdit = null;
        do{
            System.out.println("Silahkan Masukkan Reservation ID");
            reserveID = scanner.nextLine();

            for (Reservation rte : reservationList) {
                if (rte.getReservationId().equalsIgnoreCase(reserveID)) {
                    reservationToEdit = rte;
                    break;
                }else{
                    System.out.println("Tidak ada resevasi dengan ID tersebut");
                }
            }

        }while(reservationToEdit == null);

        do{
            System.out.println("Selesaikan Reservasi: ");
            reserveEnd = scanner.nextLine();
            if (reserveEnd.equalsIgnoreCase("finish") || reserveEnd.equalsIgnoreCase("cancel")) {
                reservationToEdit.setWorkstage(reserveEnd);
                System.out.println("Reservasi dengan id " + reservationToEdit.getReservationId()+ " sudah "+ reserveEnd);
                break;
            }
            
        }while(!reserveEnd.equalsIgnoreCase("finish") || !reserveEnd.equalsIgnoreCase("cancel"));

    }
    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
