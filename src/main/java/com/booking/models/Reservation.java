package com.booking.models;

import java.util.List;

import com.booking.interfaces.ICalculateReservationPrice;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Reservation implements ICalculateReservationPrice {
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private String workstage;
    private static int counter = 0;
    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    private double reservationPrice;
    //   workStage (In Process, Finish, Canceled)

    public Reservation(String reservationId, Customer customer, Employee employee, List<Service> services,
            String workstage) {
        this.reservationId = reservationId;
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        calculateReservationPrice();
        this.workstage = workstage;
    };

    

    public double getReservationPrice() {
        return reservationPrice;
    }

    public void setReservationPrice(double reservationPrice) {
        this.reservationPrice = reservationPrice;
    }



    @Override
    public void calculateReservationPrice(){
        double totalPrice = 0;
        for (Service service : services) {
            totalPrice = totalPrice + service.getPrice();
        }
        if (customer.getMember().getMembershipName().equalsIgnoreCase("none")) {
            totalPrice = totalPrice - (totalPrice * NONE_DISKON);
        }else if (customer.getMember().getMembershipName().equalsIgnoreCase("silver")) {
            totalPrice = totalPrice - (totalPrice * SILVER_DISKON);
        }else if (customer.getMember().getMembershipName().equalsIgnoreCase("gold")) {
            totalPrice = totalPrice - (totalPrice * NONE_DISKON);
        }
        setReservationPrice(totalPrice);
    }

    private String generateNewReservationID(){
        counter++;
        String formattedID = String.format("%02d", counter);
        return "Rsv-" + 
        formattedID;
    }

    public void setNewReservationID(){
        this.reservationId = generateNewReservationID();
    }

    // public String getWorkstage(){
    //     return "In Process";
    // }

    // public void setWorkstage(){
    //     this.workstage = getWorkstage();
    // }
}
