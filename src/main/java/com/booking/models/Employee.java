package com.booking.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Employee extends Person {
    private int experience;

    @Override
    public String toString() {
        return "Employee [experience=" + experience + ", getAddress()=" + getAddress() + ", getId()=" + getId()
                + ", getName()=" + getName() + ", getExperience()=" + getExperience() + ", getClass()=" + getClass()
                + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
    }

    
}
